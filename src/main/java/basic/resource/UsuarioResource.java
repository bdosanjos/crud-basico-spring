package basic.resource;


import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import basic.entity.Usuario;
import basic.entity.dto.UsuarioDTO;
import basic.entity.mappear.UsuarioMappear;
import basic.service.UsuarioService;



//Resource retorna as coisas para as páginas ou os locais de acesso.

@RestController
@RequestMapping("/user")
public class UsuarioResource {
	
	
	@Autowired
	private UsuarioService userService;
	
	
	@Autowired
	private UsuarioMappear userMap;
	
	
	
	//retornar todos os usuarios
	//Naoo tem a uri especiga então usa a padraõ do Resource
	@GetMapping
	public ResponseEntity<List<Usuario>>getUsuario(){
		List<Usuario> list = userService.getUsuario();
		return ResponseEntity.ok().body(list);
		
	}
	
	//salva um novo usuario
	@PostMapping("/salva")
	public ResponseEntity<Usuario> saveCurso(@RequestBody UsuarioDTO dto) throws URISyntaxException{
		
		Usuario novoUsuario =  userService.save(userMap.mapUsuarioDTOtoUsuario(dto));  
		return ResponseEntity.created(new URI("/cursos/salva/"+novoUsuario.getId())).body(novoUsuario);
		
	}
	
	//Busca apenas pelo nome completo
	@GetMapping("/nome")
	public  ResponseEntity<List<Usuario>> findbyNome(@RequestParam String nome){
		List<Usuario> list = userService.findByNome(nome);
		return ResponseEntity.ok().body(list);
		
	}
	
	//Busca olhando a apenas uma parte do nome
	@GetMapping("/nomeLike")
	public  ResponseEntity<List<Usuario>> findbyNomeLike(@RequestParam String nome){
		List<Usuario> list = userService.findByNomeLike("%"+nome+"%");
		return ResponseEntity.ok().body(list);
		
	}
	
	
	//Busca por email completo
	@GetMapping("/email")
	public ResponseEntity<List<Usuario>> findbyEmail(@RequestParam String email){
		List<Usuario> list = userService.findByEmail(email);
		return ResponseEntity.ok().body(list); 
	}
	
	
	//Busco por parte do email
	@GetMapping("/emailLike")
	public ResponseEntity<List<Usuario>> findbyEmailLike(@RequestParam String email){
		List<Usuario> list = userService.findByEmailLike("%"+email+"%");
		return ResponseEntity.ok().body(list);
		
		
	}
	
	
	//TODO pesquisar como lidar com as excessões
	//upodate por baseado no id
	@PutMapping("/{id}")
	public ResponseEntity<Usuario> updateUsuario(@RequestBody UsuarioDTO dto, @PathVariable Integer id) throws URISyntaxException{
				
		Usuario novo = userMap.mapUsuarioDTOtoUsuario(dto);
		novo.setId(id);
		userService.update(novo);		
		return ResponseEntity.noContent().build();			
	}
	
	
		
	//detele por id
	@DeleteMapping("/{id}")
	public ResponseEntity<Usuario> deleteUsuario(@PathVariable Integer id) throws URISyntaxException{
		userService.deleteById(id);
		return ResponseEntity.noContent().build();
	
	}
	
	//Teste de login usando o usuario
	//TODO Criar em formato de sessão
	@PostMapping("/login")
	public ResponseEntity<String> logUser(@RequestBody Usuario user) {
		List<Usuario> list = userService.getUsuario();
		for (Usuario next : list) {
			if (next.getEmail().equals(user.getEmail())	&& next.getSenha().equals(user.getSenha())) {
				return ResponseEntity.ok().body("Sucess");
				
			}
			
		}
		return ResponseEntity.ok().body("Fail");
		
	}
	
	
	
	
	
	
	
	

}
