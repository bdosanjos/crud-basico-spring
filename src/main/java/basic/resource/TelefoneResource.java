package basic.resource;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import basic.entity.Telefone;
import basic.service.TelefoneService;
import net.minidev.json.JSONObject;

@RestController
@RequestMapping("/tele")
public class TelefoneResource {
	
	@Autowired
	private TelefoneService telService;
	
	
	
	//Transformação em Json na mão, não sei se é a melhor pratica, mas evito de andar com dados desnecessarios
	private List<JSONObject> createJson(List<Object[]> list){
		
		List<JSONObject> jsonlist = new ArrayList<JSONObject>();
		  for (Object[] result : list) {
			  JSONObject json = new JSONObject();
			  json.put("ddd", result[0]);
			  json.put("numero", result[1]);
			  jsonlist.add(json);
		  }
		  return jsonlist;
		
	}
	
	@GetMapping
	public ResponseEntity<List<Telefone>>getTelefones(){
		List<Telefone> list = telService.getTelefones();
		return ResponseEntity.ok().body(list);
	}
	
	@GetMapping("/telporuserid")
	public ResponseEntity<List<JSONObject>>getTelefonesUserId(@RequestParam Integer id) throws JsonProcessingException{
		List<Object[]> list = telService.getTelefonesUsuario(id); 

		return ResponseEntity.ok().body(createJson(list));
	}
	
	@GetMapping("/telporusernome")
	public ResponseEntity<List<JSONObject>>getTelegoneUserNomeLike(@RequestParam String likeNome){
		
		List<Object[]> list = telService.getTelefonesUsuarioNome(likeNome);
		
		return ResponseEntity.ok().body(createJson(list));
		
	}
		
	

}
