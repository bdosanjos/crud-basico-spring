package basic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import basic.entity.Telefone;

public interface TelefoneReposository extends JpaRepository<Telefone,Integer> {
	

	@Query(value = "Select telefone.ddd, telefone.numero From telefone INNER JOIN usuario ON usuario.id = telefone.usuario_id WHERE usuario.id = :user",
			nativeQuery = true)
	public List<Object[]> findUsuarios(@Param( "user") Integer user);
	
	
	
	@Query(value = "SELECT telefone.ddd, telefone.numero FROM telefone INNER JOIN usuario ON usuario.id = telefone.usuario_id WHERE usuario.nome LIKE %:likeNome%",
			nativeQuery = true)
	public List<Object[]> findUsuarioNomeLike(@Param("likeNome") String likeNome);
	

}
