package basic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;


import basic.entity.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario,Integer> {
	
	//Busca por nome
	List<Usuario> findUsuarioByNome(String nome);
	//Buscar por email
	List<Usuario> findUsuarioByEmail(String email);
	//Buscar por parte do nome
	List<Usuario> findUsuarioByNomeLike(String nome);
	
	List<Usuario> findUsuarioByemailLike(String email);



}
