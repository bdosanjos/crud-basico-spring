package basic.service;

import java.util.List;


import basic.entity.Telefone;

public interface TelefoneService {
	
	
	public List<Telefone> getTelefones();
	

	public List<Object[]> getTelefonesUsuario(Integer id);
	
	public List<Object[]> getTelefonesUsuarioNome(String likeNome);

}
