package basic.service;

import java.util.List;

import basic.entity.Usuario;

public interface UsuarioService {
	
	
	public List<Usuario> getUsuario();

	public Usuario save(Usuario user);

	public Usuario findById(Integer id);

	public List<Usuario> findByNome(String nome);
	
	public List<Usuario> findByNomeLike(String nome);
	
	public List<Usuario> findByEmail(String email);
	
	public List<Usuario> findByEmailLike(String email);

	public void update(Usuario user);
	
	public void deleteById(Integer id);

	

}
