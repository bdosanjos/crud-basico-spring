package basic.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import basic.entity.Telefone;
import basic.repository.TelefoneReposository;
import basic.service.TelefoneService;


@Service
public class TelefoneServiceImpl implements TelefoneService {
	
	@Autowired
	private TelefoneReposository telRepository;

	@Override
	public List<Telefone> getTelefones() {
		
		return telRepository.findAll();
	}

	@Override
	public List<Object[]> getTelefonesUsuario(Integer id) {
		
		return telRepository.findUsuarios(id);
	}

	@Override
	public List<Object[]> getTelefonesUsuarioNome(String likeNome) {
		
		return telRepository.findUsuarioNomeLike(likeNome);
	}

}
