package basic.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import basic.entity.Usuario;
import basic.repository.UsuarioRepository;
import basic.service.UsuarioService;




@Service
public class UsuarioServiceImpl implements UsuarioService {
	
	@Autowired
	UsuarioRepository usuarioRepository;

	@Override
	public List<Usuario> getUsuario() {
		
		return usuarioRepository.findAll();
	}

	@Override
	public Usuario save(Usuario user) {
		
		return usuarioRepository.save(user);
	}

	@Override
	public Usuario findById(Integer id) {
		Optional<Usuario> user =  usuarioRepository.findById(id);
		return user.orElse(null);
	}

	@Override
	public List<Usuario> findByNome(String nome) {

		return usuarioRepository.findUsuarioByNome(nome);
	}

	@Override
	public void update(Usuario novo) {
		Usuario now = this.findById(novo.getId());
		//Ideal é separar cada um em métodos especificos
		now.setNome(now.getNome());
		now.setEmail(now.getEmail());
		now.setSenha(now.getSenha());
		
		usuarioRepository.save(now);
		
	}

	@Override
	public void deleteById(Integer id) {
		usuarioRepository.deleteById(id);
		
	}

	@Override
	public List<Usuario> findByNomeLike(String nome) {
		
		return usuarioRepository.findUsuarioByNomeLike(nome);
	}

	@Override
	public List<Usuario> findByEmail(String email) {
		
		return usuarioRepository.findUsuarioByEmail(email);
	}

	@Override
	public List<Usuario> findByEmailLike(String email) {
		
		return usuarioRepository.findUsuarioByemailLike(email);
	}

}
