package basic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import basic.entity.Telefone;
import basic.entity.Usuario;
import basic.repository.TelefoneReposository;
import basic.repository.UsuarioRepository;


@Component
public class DataBaseFill implements CommandLineRunner{
	

	//TODO criação de testes
	
	@Autowired
	private UsuarioRepository userRepository;
	
	@Autowired
	private TelefoneReposository telRepostory;
	
	@Override
	public void run(String... args) throws Exception {
		
		Usuario userTesteUm = new Usuario("Bruno","emailteste@gmail.ccom","senhabonita");
		Usuario userTesteDois = new Usuario("Nani","nanidanonie@gmail.ccom","23456578");
		Usuario userTesteTres = new Usuario("Anie","AnneAnie@gmail.ccom","amelhorbandadetodosostemposdaultimasemana");
		userRepository.save(userTesteUm);
		userRepository.save(userTesteDois);
		userRepository.save(userTesteTres);		
		
		Telefone telTesteUm = new Telefone("2345678",82,userTesteUm);
		Telefone telTesteDois = new Telefone("8756432",81,userTesteUm);
		Telefone telTesteTres = new Telefone("645392785",81,userTesteDois);
		telRepostory.save(telTesteUm);
		telRepostory.save(telTesteDois);
		telRepostory.save(telTesteTres);
		
		
		
	}
	

}
