package basic.entity.mappear;

import org.springframework.stereotype.Service;

import basic.entity.Telefone;
import basic.entity.dto.TelefoneDTO;

@Service
public class TelefoneMappear {
	
	public Telefone mapTelefoneDTotoTelefone(TelefoneDTO dto) {
		Telefone tel = new Telefone(dto.getNumero(),dto.getDdd(),dto.getUsuario());
		return tel;
	}

}
