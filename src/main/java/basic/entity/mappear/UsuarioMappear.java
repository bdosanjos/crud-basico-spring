package basic.entity.mappear;

import org.springframework.stereotype.Service;

import basic.entity.Usuario;
import basic.entity.dto.UsuarioDTO;

@Service
public class UsuarioMappear {
	
	public Usuario mapUsuarioDTOtoUsuario(UsuarioDTO dto ) {
		Usuario user = new Usuario(dto.getNome(),dto.getEmail(),dto.getSenha());
		return user;
		
	}

}
