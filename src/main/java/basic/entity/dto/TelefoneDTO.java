package basic.entity.dto;


import java.io.Serializable;

import org.springframework.lang.NonNull;

import basic.entity.Usuario;

public class TelefoneDTO implements Serializable {
	
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	private  Integer id; 
	
	
	@NonNull
	private String numero; 
	
	
	@NonNull
	private Integer ddd;
	
	private Usuario usuario;


	public TelefoneDTO() {
		
	}


	public Usuario getUsuario() {
		return usuario;
	}


	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public TelefoneDTO(Integer id, String numero, Integer ddd, Usuario usuario) {
		super();
		this.id = id;
		this.numero = numero;
		this.ddd = ddd;
		this.usuario = usuario;
	}



	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getNumero() {
		return numero;
	}


	public void setNumero(String numero) {
		this.numero = numero;
	}


	public Integer getDdd() {
		return ddd;
	}


	public void setDdd(Integer ddd) {
		this.ddd = ddd;
	}


	
	
	
	

}
