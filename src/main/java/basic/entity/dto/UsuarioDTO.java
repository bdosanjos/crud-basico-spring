package basic.entity.dto;

import java.io.Serializable;

import org.springframework.lang.NonNull;



public class UsuarioDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	
	private  Integer id;  
	@NonNull
	private String nome; 
	@NonNull
	private String email; 
	@NonNull
	private String senha;
	
	
	
	
	public UsuarioDTO() {
		
	}
	
	
	
	public UsuarioDTO(Integer id, String nome, String email, String senha) {
		super();
		this.id = id;
		this.nome = nome;
		this.email = email;
		this.senha = senha;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	} 
	
	
	

}
